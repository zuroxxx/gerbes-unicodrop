<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {

        sleep(1);

        $products = Product::all();
        return response()->json($products);
    }

    public function show($id)
    {

        try {

            // Simular delay
            sleep(1);

            $product = Product::find($id);

            if (!$product) {
                return response()->json(['error' => 'Produto não encontrado'], 404);
            }

            return response()->json($product);

        }catch ( \Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }

    }

    public function store(Request $request)
    {
        try {

            // Simular delay
            sleep(1);

            $validatedData = $request->validate([
                'name' => 'required|string|min:3|max:255',
                'price' => 'required|numeric',
                'description' => 'nullable|min:3|string',
            ]);

            $product = Product::create($request->all());
            return response()->json($product, 201);

        }catch ( \Exception $e) {

            if( $e instanceof \Illuminate\Validation\ValidationException){
                return response()->json([
                    'error' => $e->getMessage(),
                    'errors' => $e->errors()
                ], 500);
            }

            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {

            // Simular delay
            sleep(1);

            $product = Product::find($id);

            if (!$product) {
                return response()->json(['error' => 'Produto não encontrado'], 404);
            }

            $validatedData = $request->validate([
                'name' => 'required|string|min:3|max:255',
                'price' => 'required|numeric',
                'description' => 'nullable|min:3|string',
            ]);

            $product->update($request->all());
            return response()->json($product, 200);

        }catch ( \Exception $e) {

            if( $e instanceof \Illuminate\Validation\ValidationException){
                return response()->json([
                    'error' => $e->getMessage(),
                    'errors' => $e->errors()
                ], 500);
            }

            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {

        try {

            // Simular delay
            sleep(1);

            $product = Product::find($id);

            if (!$product) {
                return response()->json(['error' => 'Produto não encontrado'], 404);
            }

            $product->delete();
            return response()->json(null, 204);

        }catch ( \Exception $e) {

            if( $e instanceof \Illuminate\Validation\ValidationException){
                return response()->json([
                    'error' => $e->getMessage(),
                    'errors' => $e->errors()
                ], 500);
            }

            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
