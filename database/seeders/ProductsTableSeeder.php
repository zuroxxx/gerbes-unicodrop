<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        if( Product::count() > 0 ) {
            return;
        }

        for ($i = 1; $i <= 10; $i++) {
            Product::create([
                'name' => "Product $i",
                'description' => "Description for Product $i",
                'price' => rand(10, 100),
            ]);
        }
    }
}
