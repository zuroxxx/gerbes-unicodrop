<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Products Manager</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>
<div id="app"></div>
@vite(['resources/js/manager.products.js'])
<div id="app"></div>
</body>
</html>
