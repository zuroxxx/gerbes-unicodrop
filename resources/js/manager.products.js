/**
 * Gerenciador de Produtos
 * Este componente é responsável por administrar os produtos por meio da API.
 */
import { createApp, h } from 'vue';
import ProductsCrud from './componenets/ProductsCrud.vue';
const app  = createApp({
    render: ()=>h(ProductsCrud),
});
app.mount('#app');



