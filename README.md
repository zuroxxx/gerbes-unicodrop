### Gerbes Matos @ Unicodrop

Desafio técnico para a vaga de desenvolvedor na Unicodrop.

## Informações

Utilizei o Vue.js 3, apesar de não ter experiência anterior com ele, tendo feito apenas alguns testes de estudo. Por isso, acabei desenvolvendo a aplicação no formato do Vue.js 2.

A estrutura de produtos eu fiz a mais simples possível, apenas para atender o desafio.

A documentação da API pode ser visualizada em OpenAPI 3.0 no arquivo `open-api.yaml` na raiz do projeto.

## Instalação
Para instalar o projeto Laravel
```bash
composer install
```

Para compilar o projeto Vue.js
```bash
npm install
npm run build
```

## Configuração
Para configurar o projeto laravel, copie o arquivo `.env.example` para `.env` e configure o banco de dados.

## Migrations
Para criar as tabelas do banco de dados, execute o comando:
```bash
php artisan migrate
```

## Seeders
Para popular o banco de dados com dados de teste, execute o comando:
```bash
php artisan db:seed
```

## Servidor
Para iniciar o servidor, execute o comando:
```bash
php artisan serve
```
